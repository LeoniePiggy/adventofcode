

def _10_1():
    number_stream = set([])
    distances = [3, ]
    with open("d10/d10.input", "r") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                number_stream.add(int(ln))
            else:
                number_stream.add(0)
                number_stream = sorted(number_stream)
                for index, val in enumerate(number_stream):
                    try:
                        distances.append(number_stream[index + 1] - number_stream[index])
                    except IndexError:
                        pass
                print(f"Result (10/1):\n1-jolt diff: {distances.count(1)}\n3-jolt diff: {distances.count(3)}\nproduct: {distances.count(1) * distances.count(3)}\n{10*'=='}")
    return number_stream


def _10_2():
    number_stream = _10_1()
    adapters_dict = {}

    def count_paths(start, end=max(number_stream), path=number_stream):
        possible_paths = 0
        for step in range(1, 3 + 1):
            next_adapter = start + step
            if next_adapter == end:
                return possible_paths + 1
            elif next_adapter in adapters_dict.keys():
                possible_paths += adapters_dict[next_adapter]
            elif next_adapter in path:
                return_value = count_paths(next_adapter)
                possible_paths += return_value
                adapters_dict.update({
                    next_adapter: return_value
                })
        return possible_paths

    print(count_paths(0))
