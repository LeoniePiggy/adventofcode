import re


def _5_1():
    seat_ids = []
    with open("d05/d05.input", "r") as f:
        for ln_index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln:
                ln = re.sub(r"[FL]", "0", ln)
                ln = re.sub(r"[BR]", "1", ln)
                seat_id = int(ln, 2)
                seat_ids.append(seat_id)
        print(max(seat_ids))
    return seat_ids


def _5_2():
    seat_ids = _5_1()
    seat_ids.sort()
    for i in range(max(seat_ids)):
        if i not in seat_ids and i/8 > 10:
            print(i)
