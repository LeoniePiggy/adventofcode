

def _13_1():
    with open("d13/d13.input", "r") as f:
        for index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln and ln is not "-":
                if index == 0:
                    timestamp = int(ln)
                elif index == 1:
                    bus_ids = ln.split(",")
                    bus_ids = list(filter(lambda a: a != "x", bus_ids))
                    bus_ids = [int(val) for val in bus_ids]
            else:
                bus_times = {}
                for val in bus_ids:
                    bus_times[(((timestamp - timestamp % val) + val) - timestamp)] = val
                print(bus_times[sorted(bus_times)[0]] * sorted(bus_times)[0])


def _13_2():
    with open("d13/d13.example", "r") as f:
        for index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln and ln is not "-":
                if index == 1:
                    bid_list = ln.split(",")
                    bid_list = [None if val == "x" else int(val) for val in bid_list]
                    tmp_bid_dict = {}
                    bid_dict = {}
                    for i, val in enumerate(bid_list):
                        if val:
                            tmp_bid_dict.update({
                                i: val,
                            })
                    for index, item in enumerate(tmp_bid_dict.items()):
                        key, val = item
                        bid_dict.update({
                            index: {
                                key: val
                            }
                        })

            else:
                print(bid_dict)
