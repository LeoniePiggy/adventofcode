

def _9_1():
    preamble = 5
    number_stream = []
    with open("d09/d09.input", "r") as f:
        if f.name.endswith("input"):
            preamble += 20
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                number_stream.append(int(ln))
            else:
                i = preamble
                while i <= len(number_stream):
                    subject_sum = number_stream[i]
                    list_to_sum = number_stream[i-preamble:i]
                    can_sum_to_subject = []

                    for a in list_to_sum:
                        for b in list_to_sum:
                            if a + b == subject_sum:
                                can_sum_to_subject.append(True)
                            else:
                                can_sum_to_subject.append(False)
                    if not any(can_sum_to_subject):
                        print(subject_sum)
                        return subject_sum
                    i += 1


def _9_2():
    preamble = 5
    number_stream = []
    with open("d09/d09.input", "r") as f:
        if f.name.endswith("input"):
            preamble += 20
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                number_stream.append(int(ln))
            else:
                invalid_number = _9_1()
                for index, val in enumerate(number_stream):
                    test_sum = 0
                    summed_numbers = []
                    internal_index = index
                    while test_sum < invalid_number:
                        test_sum += number_stream[internal_index]
                        summed_numbers.append(number_stream[internal_index])
                        internal_index += 1
                    if test_sum == invalid_number:
                        print(f"Min: {min(summed_numbers)}\nMax: {max(summed_numbers)}\nSum: {min(summed_numbers) + max(summed_numbers)}")
                        break
